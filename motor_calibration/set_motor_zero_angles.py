from dynamixel_controller import Dynamixel
import json

def main():
    json_file = "../helper_functions/motor.config.json"
    motor_config = json.load(open(json_file))

    ids = []
    series = []
    for i in range(12):
        ids.append(i)
        series.append("xm")

    block = Dynamixel(ID = ids, descriptive_device_name="Dynamixel block", 
                        series_name=series, baudrate=3000000, port_name="/dev/ttyUSB0")

    block.begin_communication(enable_torque=False)

    for conf in motor_config.values():
        print("old angle:", conf["zero_angle"], end = "  ")
        curr_pos = block.read_position(conf["ID"])
        zero_angle = (curr_pos - 2048) * 360 / 4096
        conf["zero_angle"] = int(zero_angle)
        print("new angle:", conf["zero_angle"])

    block.end_communication()

    # Serializing json
    json_object = json.dumps(motor_config, indent=4)
    
    # Writing to sample.json
    with open(json_file, "w") as outfile:
        outfile.write(json_object)

if __name__ == "__main__":
    main()