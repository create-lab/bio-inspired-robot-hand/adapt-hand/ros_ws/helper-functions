# Motor calibration sequence

1. Go to dynamixel wizard and move fingers to match the picture - calibration.jpg
2. Go to *motor_configuration.json* and edit the *direction* field for each motor. 
    - *cw* means the positive direction is when the motor moves in a clockwise direction. 
    - *ccw* is counter-clockwise. 
    - For the finger and thumb abduction, refer to calibration.jpg to know what the positive direction is.
3. Run *set_motor_zero_angles.py* to set the motor angles in the *motor_configuration.json* file automatically.