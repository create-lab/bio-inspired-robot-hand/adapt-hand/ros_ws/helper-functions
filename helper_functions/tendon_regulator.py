import os
import json
import numpy as np
from copy import deepcopy as cp

class TendonRegulator:
    def __init__(self) -> None:

        self.tendon_array = {   "index_base":0, "index_flexure":0, "middle_base":0,"middle_flexure":0,
                                "ring_base":0,"ring_flexure":0, "pinky_base":0,"pinky_flexure":0, 
                                "thumb_base":0,"thumb_base_orth":0,"thumb_flexure":0, "orthogonal":0
                            }
        dir_path =  os.path.dirname(os.path.realpath(__file__))
        json_file = dir_path + "/hand.config.json"
        
        self.hand_config = json.load(open(json_file))
        self.tendon_names = self.tendon_array.keys()

        self.__copy_tendon_limits()

    def __copy_tendon_limits(self):
        self.tendon_limits = {}

        for finger_name in ["index", "middle", "ring", "pinky", "thumb", "orthogonal"]:
            limits = self.hand_config[finger_name]["tendon limit"]
            for key, value in zip(limits.keys(), limits.values()):
                self.tendon_limits[key] = {"min":value[0], "max":value[1], "range": value[1] - value[0]}
            
    def get_tendon_limits(self):
        return self.tendon_limits
    
    def get_empty_tendon_array(self):
        return self.tendon_array
    
    def check_tendon_limits(self, demand):
        limited_demand = cp(self.tendon_array)

        for tendon_name in self.tendon_names:
            if demand[tendon_name] < self.tendon_limits[tendon_name]["min"]:
                limited_demand[tendon_name] = cp(self.tendon_limits[tendon_name]["min"])
            
            elif demand[tendon_name] > self.tendon_limits[tendon_name]["max"]:
                limited_demand[tendon_name] = cp(self.tendon_limits[tendon_name]["max"])

            else:
                limited_demand[tendon_name] = cp(demand[tendon_name])

        return limited_demand

    def tendon_value_list_to_dict(self, tendon_list):
        tendon_dict = cp(self.tendon_array)
        for i, key in enumerate(self.tendon_array.keys()):
            tendon_dict[key] = tendon_list[i]
        
        return tendon_dict
        