import os
import json
import sys
import numpy as np
from copy import deepcopy as cp
import scipy.optimize

from sensor_msgs.msg import JointState

from .tendon_regulator import TendonRegulator
from .utility import *

class HandRegulator:

    def __init__(self) -> None:        
        dir_path =  os.path.dirname(os.path.realpath(__file__))
        
        json_file = dir_path + "/hand.config.json"
        self.hand_config = json.load(open(json_file))

        json_file = dir_path + "/motor.config.json"
        self.motor_config = json.load(open(json_file))

        self.tendon_regulator = TendonRegulator()

        # Magical angle correction ratio for orth joints
        self.orth_angle_correction = -1/30

    
    def _get_hand_joint_names(self):
        joint_names = []
        for key, value in zip(self.hand_config.keys(), self.hand_config.values()):
            if key == "orthogonal":
                joint_names += value["joint_name"]
            else:
                joint_names.append(value["joint_name"]["pin"])
                joint_names += value["joint_name"]["flex"]

                if key == "thumb":
                    joint_names.append(value["joint_name"]["orth"])

        return joint_names

    def _flex_dt_given_theta(self, theta, joint_length, tendon_lever, length_wo_t):
        if theta == 0:
            tendon_position = 0
        else:
            base_t = joint_length + 2 * length_wo_t
            current_t = 2 * (joint_length / theta - tendon_lever) * np.sin(theta / 2) + 2 * length_wo_t * np.cos(theta / 2)
            tendon_position = base_t - current_t

        return tendon_position
    
    def _theta_given_flex_dt(self, tendon_position, joint_length, tendon_lever, length_wo_t):

        def f(theta, joint_length, tendon_lever, length_wo_t):
            return self._flex_dt_given_theta(theta, joint_length, tendon_lever, length_wo_t) - tendon_position
        
        is_success = False
        for i in range(5):
            rand = 2 * cp(np.random.random()) -1
            scale = 1 + rand * 0.2
            limits = [(-np.pi/3)*scale, (np.pi/0.8)*scale]
            try:
                solution = scipy.optimize.root_scalar(f, args=(joint_length, tendon_lever, length_wo_t),
                                                    bracket = [-np.pi/3, np.pi/0.8], method = "brentq")
                is_success = True
                break
            except:
                print("\n\nError with brentq! trying new variable!  Old variable:", limits)

        if is_success == False:
            sys.exit()
            
        return solution.root
    
    def _dt_given_pin_joint_angle(self, angle, name, is_thumb_orth = False):
        joint_type = "pin"
        if is_thumb_orth:
            joint_type = "orth"
            
        radius = self.hand_config[name]["radius"][joint_type]
        dt = angle * radius

        return dt
    
    def _dt_given_orthogonal_angles(self, index_angle, ring_angle, pinky_angle):
        # Magical angle correction ratio
        self.orth_angle_correction = -1/50

        motion_ratio = self.hand_config["orthogonal"]["motion_ratio"]

        index_tendon = index_angle / self.orth_angle_correction / motion_ratio["Index"]
        ring_tendon = ring_angle / self.orth_angle_correction / motion_ratio["Ring"] * -1
        pinky_tendon = pinky_angle / self.orth_angle_correction / motion_ratio["Pinky"] * -1
        
        dt = (index_tendon + ring_tendon + pinky_tendon) / 3
        
        return dt
    
    def _calculate_pin_joint_angle_from_tendon(self, tendon_position, name, is_thumb_orth = False):
        joint_type = "pin"
        if is_thumb_orth:
            joint_type = "orth"
            
        radius = self.hand_config[name]["radius"][joint_type]

        joint_name_list = [self.hand_config[name]["joint_name"][joint_type]]
        joint_angle_list = [tendon_position / radius]

        return joint_name_list, joint_angle_list
            
        
    def _calculate_double_flexure_joint_angle_from_tendon(self, tendon_position, name):
        
        joint_name_list = self.hand_config[name]["joint_name"]["flex"]
        joint_angle_list = []
        
        simple_joint_name_list = cp(joint_name_list)
        for i, item in enumerate(simple_joint_name_list):
            temp = item.split("_")
            simple_joint_name_list[i] = temp[1]

        stiffnss_config = self.hand_config[name]["stiffness_ratio"]

        for simple_joint_name in simple_joint_name_list:
            
            ratio = stiffnss_config[simple_joint_name] / sum(stiffnss_config.values())
            tendon_position_for_joint = tendon_position * ratio

            angle = self._theta_given_flex_dt(tendon_position_for_joint, self.hand_config[name]["joint_length"], 
                                               self.hand_config[name]["tendon_lever"], self.hand_config[name]["length_wo_tendon"])
            
            joint_angle_list.append(angle)

        return joint_name_list, joint_angle_list

    def _calculate_orthogonal_coupled_joint_angle_from_tendon(self, tendon_position):
        
        # Magical angle correction ratio
        self.orth_angle_correction = -1/30

        motion_ratio = self.hand_config["orthogonal"]["motion_ratio"]
        joint_name_list = self.hand_config["orthogonal"]["joint_name"]
        joint_angle_list = []

        for joint_name in joint_name_list:
            
            if "Index" in joint_name:
                joint = tendon_position * self.orth_angle_correction * motion_ratio["Index"]
            elif "Ring" in joint_name:
                joint = tendon_position * self.orth_angle_correction * motion_ratio["Ring"]
            elif "Pinky" in joint_name:
                joint = tendon_position * self.orth_angle_correction * motion_ratio["Pinky"]
            else:
                joint = 0.0
            
            joint_angle_list.append(joint)
    
        return joint_name_list, joint_angle_list


    # Public methods
    def tendon_pos_to_motor_pos(self, tendon_demand):    
        motor_position = [0] * 12

        for i, motor in enumerate(self.motor_config.values()):
            tendon_position = tendon_demand[motor["tendon_name"]]
            if motor["direction"] == "ccw":
                direction = 1
            else:
                direction = -1

            motor_position[i] = float((tendon_position / motor["radius"]) * direction + np.radians(motor["zero_angle"]) )

        return motor_position
    
    def motor_pos_to_tendon_pos(self, motor_msg : JointState):
        tendon_position = self.tendon_regulator.get_empty_tendon_array()

        for name, pos in zip(motor_msg.name, motor_msg.position):
            motor_conf = self.motor_config[name]
            target_tendon = motor_conf["tendon_name"]

            if motor_conf["direction"] == "ccw":
                direction = 1
            else:
                direction = -1
            
            tendon_position[target_tendon] = (float(pos) - np.radians(motor_conf["zero_angle"])) * direction * motor_conf["radius"]

        return tendon_position
    
    def tendon_pos_to_hand_pos(self, tendon_position:dict):
        hand_joint_information = {"name":[], "angle":[]}

        for name, pos in zip(tendon_position.keys(), tendon_position.values()):
            key = name.split("_")

            if key[0] == "orthogonal":
                joint_name_list, joint_angle_list = self._calculate_orthogonal_coupled_joint_angle_from_tendon(pos)

            elif key[1] == "base":
                is_thumb_orth = False
                if len(key) == 3:
                    is_thumb_orth = True
                    
                joint_name_list, joint_angle_list = self._calculate_pin_joint_angle_from_tendon(pos, key[0], is_thumb_orth=is_thumb_orth)
               
            else:
                joint_name_list, joint_angle_list = self._calculate_double_flexure_joint_angle_from_tendon(pos, key[0])

            hand_joint_information["name"] += joint_name_list
            hand_joint_information["angle"] += joint_angle_list

        return hand_joint_information, tendon_position

    def motor_pos_to_hand_pos(self, motor_msg : JointState):
        tendon_position = self.motor_pos_to_tendon_pos(motor_msg)
        hand_joint_information, tendon_position = self.tendon_pos_to_hand_pos(tendon_position)

        return hand_joint_information, tendon_position
    

    def hand_pos_to_tendon_pos(self, hand_pos):
        tendon_position = self.tendon_regulator.get_empty_tendon_array()

        fingers = ["Thumb", "Index", "Middle", "Ring", "Pinky"]

        # Flex
        for finger in fingers:
            if finger != "Thumb":
                avg_flex_angle = 0.5 * (hand_pos[finger + "_PIP"] + hand_pos[finger + "_DIP"])
            else:
                avg_flex_angle = 0.5 * (hand_pos[finger + "_MCP"] + hand_pos[finger + "_IP"])

            dt = self._flex_dt_given_theta(avg_flex_angle, 
                                            self.hand_config[finger.lower()]["joint_length"],
                                            self.hand_config[finger.lower()]["tendon_lever"], 
                                            self.hand_config[finger.lower()]["length_wo_tendon"])
            
            tendon_position[finger.lower() + "_flexure"] = float(dt * 2)

        # Base
        pin_joints = ["Thumb_CMC_Orth", "Thumb_CMC", "Index_MCP", "Middle_MCP", "Ring_MCP", "Pinky_MCP"]
        for pin_joint in pin_joints:
            name = pin_joint.split("_")[0].lower()

            if "Orth" in pin_joint:
                dt = self._dt_given_pin_joint_angle(hand_pos[pin_joint], name, True)
                tendon_name = name + "_base_orth"
            else:
                dt = self._dt_given_pin_joint_angle(hand_pos[pin_joint], name, False)
                tendon_name = name + "_base"

            tendon_position[tendon_name] = float(dt)

        # Orthogonal
        dt = self._dt_given_orthogonal_angles(hand_pos["Index_MCP_Orth"], hand_pos["Ring_MCP_Orth"], hand_pos["Pinky_MCP_Orth"])
        tendon_position["orthogonal"] = float(dt)

        return tendon_position