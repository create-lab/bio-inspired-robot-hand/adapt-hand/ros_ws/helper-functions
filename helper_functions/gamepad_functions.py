from sensor_msgs.msg import Joy
import time
from copy import deepcopy as cp

class GamepadFunctions:
    def __init__(self) -> None:

        self.button_labels = [("cross", 0), ("circle", 1), ("triangle", 2), ("square", 3), 
                            ("up", None), ("down", None), ("left", None), ("right", None), 
                            ("R1", 5), ("R2", 7), ("L1", 4), ("L2", 6), 
                            ("share", 8), ("options", 9), ("start", 10), ("joy right", 12), ("joy left", 11)]

        self.axis_data = {"Lx":0, "Ly":0, "Ltrigger":-1, "Rx":0, "Ry":0, "Rtrigger":-1}

        self.button_data, self.button_correspondance = self.__populate_button_dictionary()
        self.button_pressed = cp(self.button_data)

    def __populate_button_dictionary(self):
        dictionary = {}
        button_correspondance = {}
        for label in self.button_labels:
            dictionary[label[0]] = 0
            if label[1] is not None:
                button_correspondance[label[1]] = label[0]

        return dictionary, button_correspondance
    
    def convert_joy_msg_to_dictionary(self, joy:Joy):
        for dict_key, ax in zip(self.axis_data.keys(), joy.axes):
            self.axis_data[dict_key] = ax

        for dict_key, but in zip(self.button_data.keys(), joy.buttons):
            self.button_data[dict_key] = but      

    def if_button_pressed(self, name):
        if self.button_data[name] == 1 and self.button_pressed[name] == 0:
            self.button_pressed[name] = 1
            return False
        
        elif self.button_data[name] == 0 and self.button_pressed[name] == 1:
            self.button_pressed[name] = 0
            time.sleep(0.01)
            return True

        else:
            return False